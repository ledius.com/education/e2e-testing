import { Builder, By, Capabilities, WebElement } from "selenium-webdriver";

import 'chromedriver';
const capabilities = Capabilities.chrome();
capabilities.set('chromeOptions', { w3c: false });
const driver = new Builder().withCapabilities(capabilities).build();

jest.setTimeout(60000);

describe('test', () => {
  beforeAll(async () => {
    await driver.get('https://google.com');
  });

  describe('find search input', () => {
    let actual: WebElement;
    beforeEach(async () => {
      actual = await driver.findElement(By.name('q'));
    });

    it('tag is input', async () => {
      expect(await actual.getTagName()).toBe('input');
    });
  });

  afterAll(async () => {
    await driver.quit();
  });
});
